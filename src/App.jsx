import { Form, Characters } from './components';
import { useState } from 'react';
import './App.scss';

const charactersFromStorage = JSON.parse(localStorage.getItem('characters')) || [];

function App() {
    const [characters, setCharacters] = useState([charactersFromStorage]);

    const addCharacter = (newCharacter) => {
        setCharacters([...characters, newCharacter]);
        localStorage.setItem('characters', JSON.stringify([...characters, newCharacter]));
    };
    const deleteCharacter = (id) => {
        // uncomment to delete by index and not by id(change the argument to index)
        // const newCharacters = [...characters];
        // newCharacters.splice(index, 1); // deletes the selected element

        const newCharacters = characters.filter((char) => char.id !== id);
        setCharacters(newCharacters);
        localStorage.setItem('characters', JSON.stringify(newCharacters));
    };

    return (
        <div className="app">
            <h1>Forms in React</h1>
            <Form addCharacter={addCharacter} />
            <Characters characters={characters} deleteCharacter={deleteCharacter} />
        </div>
    );
}

export default App;
