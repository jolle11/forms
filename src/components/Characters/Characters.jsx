import { Character } from '../../components';

const Characters = ({ characters, deleteCharacter }) => {
    return (
        <>
            <h1>Personajes</h1>
            <div style={{ display: 'flex' }}>
                {characters.map((char, index) => (
                    <Character
                        // key={`${index}-${char.name}-${char.lastName}`}
                        key={char.id}
                        character={char}
                        // index={index}
                        deleteCharacter={deleteCharacter}
                    />
                ))}
            </div>
        </>
    );
};

// key = ID, o referencia única a cada elemento
// 0-cat-woman
// 1-albus-dumbledore

export default Characters;
