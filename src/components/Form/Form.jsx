import { useState } from 'react';
import './Form.scss';

const INITIAL_STATE = {
    name: '',
    lastName: '',
    superpower: '',
    image: '',
};

const Form = (props) => {
    const [formData, setFormData] = useState(INITIAL_STATE);
    const submitForm = (event) => {
        event.preventDefault();
        event.stopPropagation();
        props.addCharacter(formData);
        console.log(formData);
        setFormData(INITIAL_STATE);
    };

    const inputChange = (event) => {
        event.preventDefault();
        setFormData({
            ...formData,
            [event.target.name]: event.target.value,
        });
        console.log('Input that changes', event.target.name);
        console.log(event.target.value);
    };

    return (
        <form onSubmit={submitForm} className="form">
            <label htmlFor="name">
                <p>Name:</p>
                <input type="text" name="name" onChange={inputChange} value={formData.name} />
            </label>
            <label htmlFor="">
                <p>Last name:</p>
                <input type="text" name="lastName" onChange={inputChange} value={formData.lastName} />
            </label>
            <label htmlFor="">
                <p>Superpower:</p>
                <input type="text" name="superpower" onChange={inputChange} value={formData.superpower} />
            </label>
            <label htmlFor="">
                <p>Image:</p>
                <input type="text" name="image" onChange={inputChange} value={formData.image} />
            </label>
            {/* This next line uses && to avoid the second condition of the ternary operator. It's like we only have the if(){} without else or else if */}
            {formData.image && <img src={formData.image} alt={formData.name}></img>}

            <button type="submit">Send Form</button>
        </form>
    );
};

export default Form;
